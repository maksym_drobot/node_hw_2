const { Note } = require('../models/notesModel');

const noteTextHelperMiddleware = (req, res, next) => {
  const { text } = req.body;

  if (!text) {
    return res.status(400).json({ message: 'Text for note can\'t be empty' });
  }

  next();
  return false;
};

const noteExistsHelperMiddleware = async (req, res, next) => {
  const { id } = req.params;

  try {
    const note = await Note.findById(
      id,
      '_id userId text completed createdDate',
    );

    if (!note) {
      return res
        .status(400)
        .json({ message: 'Note with given id is not founded' });
    }

    req.note = note;

    next();
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
  return false;
};

module.exports = {
  noteTextHelperMiddleware,
  noteExistsHelperMiddleware,
};
