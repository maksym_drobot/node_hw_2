const jwt = require('jsonwebtoken');

const {
  parsed: { SECRET_KEY },
} = require('dotenv').config({ path: './.env' });

const { User } = require('../models/userModel');

const authMiddleware = async (req, res, next) => {
  const { authorization } = req.headers;

  if (!authorization) {
    return res.status(400).json({
      message: 'Header \'authorization\' is not provided',
    });
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    return res.status(400).json({
      message: 'Token for \'authorization\' is empty',
    });
  }

  try {
    const jwtPayload = jwt.verify(token, SECRET_KEY);

    const currentUserProfile = await User.findById(jwtPayload._id);

    if (!currentUserProfile) {
      return res.status(400).json({ message: 'User profile is not founded' });
    }

    req.currentUserProfile = jwtPayload;

    next();
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
  return false;
};

module.exports = {
  authMiddleware,
};
