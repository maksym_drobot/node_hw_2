const bcrypt = require('bcryptjs');

const {
  parsed: { ENCODE_ROUNDS },
} = require('dotenv').config({ path: './.env' });

const { User } = require('../models/userModel');

const getUserProfile = async (req, res, next) => {
  try {
    return res.status(200).json({
      user: {
        _id: req.currentUserProfile._id,
        username: req.currentUserProfile.username,
        createdDate: req.currentUserProfile.createdDate,
      },
    });
  } catch (err) {
    return next(err);
  }
};

const deleteUserProfile = async (req, res) => {
  try {
    const userProfile = await User.findByIdAndDelete(
      req.currentUserProfile._id,
    );

    if (!userProfile) {
      return res.status(400).json({ message: 'User has been already removed' });
    }

    return res.status(200).json({ message: 'Success' });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

const updateUserPassword = async (req, res) => {
  const { oldPassword, newPassword } = req.body;

  const currentUserProfile = await User.findById(req.currentUserProfile._id);

  if (!bcrypt.compareSync(oldPassword, currentUserProfile.password)) {
    return res.status(400).json({ message: 'Your old password isn\'t correct' });
  }

  if (!newPassword) {
    return res.status(400).json({ message: 'New password can\'t be empty' });
  }

  try {
    const encodedNewPassword = bcrypt.hashSync(
      newPassword,
      bcrypt.genSaltSync(+ENCODE_ROUNDS),
    );

    currentUserProfile.password = encodedNewPassword;
    currentUserProfile.save().then((updatedProfile) => {
      console.log(updatedProfile);
    });

    return res.status(200).json({ message: 'Success' });
  } catch (err) {
    return res.status(400).json({ message: err.message });
  }
};

module.exports = {
  getUserProfile,
  deleteUserProfile,
  updateUserPassword,
};
