const { Schema, model } = require('mongoose');

const notesSchema = new Schema({
  userId: {
    type: String,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    required: true,
  },
  createdDate: {
    type: String,
    required: true,
  },
});

const Note = model('notes', notesSchema);

module.exports = {
  Note,
};
