const { Schema, model } = require('mongoose');

const userSchema = new Schema({
  username: {
    type: String,
    required: [true, 'Please tell us your name'],
    unique: true,
    trim: true,
  },
  password: {
    type: String,
    required: [true, 'Please provide the password'],
    minlength: 8,
  },
  createdDate: {
    type: String,
    required: true,
  },
});

const User = model('users', userSchema);

module.exports = {
  User,
};
