const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

const {
  parsed: { MONGODB_CONNECTION, PORT },
} = require('dotenv').config({ path: './.env' });

const { authRouter } = require('./routes/authRouter');
const { userRouter } = require('./routes/userRouter');
const { notesRouter } = require('./routes/notesRouter');

const { authMiddleware } = require('./middleware/authMiddleware');

const app = express();

mongoose.connect(MONGODB_CONNECTION);

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', authMiddleware, userRouter);
app.use('/api/notes', authMiddleware, notesRouter);

const start = async () => {
  try {
    app.listen(PORT || 8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

function errorHandler(err, req, res) {
  console.log(err.message);
  res.status(500).send({ message: 'Server error' });
}

app.use(errorHandler);
